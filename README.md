# Libitum application test

As a part of the application process as a developer here at libitum, we would like to see some code you have written for yourself.

Don�t spend more than your free time for a few days on this task
We do not expect everything to be perfect fot this amount of time, but do as much as you can, and try to discuss any compromises you have chosen in the readme file.
Of course, you are allowed to spend more time, to make a better solution, but this is not required. 

If you apply as a backend-developer, the focus should of course be on the backend, and the frontend can be very simplistic.
Same the other way around, if you are a frontend-developer, the backend can be as thin as possible.


## The task

You are to build a simple webshop application that can list different products.
Each product should be able to be locked or unlocked. Only unlocked products are selectable.

There must be at least three products.
From start, only one of them should be selectable, the other two should be locked.
When the selectable product is selected, the second product should unlock.
Then, when the second product is selected, the third product should be unlocked.

If product two is then unselected, the third product must automatically be both unselected and locked.
Similar, if the first product is unselected, both product two and tree must be unselected and locked.

There should be a feature to order the products where all selected products should be stored in a database.
This should also keep track of the history of orders, if any changes are made.

An example of this use case is when a customer selects products for a bathroom.
If a customer wants a bathtub, there are a different set of mixers then when a shower is used.
You should never be able to select a showermixer in combination with a bathtub and vice versa.


## Technical

This should be built in a programming language and a database technology free of coice.
The frontend should be made with HTML, CSS and JavaScript if needed.
You are free, and also encouraged, to use any frameworks or libraries you want.

It is not required but if you have experience with docker, we would love to see it containerized.


## When your finished

When you are finished you should upload the code to a public repository on github (or similar service) and email us the link to it.  
There should be a readme file explaining how to run the project and discuss any thoughts, compromises and assumptions you have made during the test.

Your code will be evaluated by some of these criteria:
* Coding style
* Consistency
* Error handling
* Documentation
* Tests
* Versioning history (eg. git commit comments)

If there are any questions, feel free to contact us.

![](libitum_logo.png)